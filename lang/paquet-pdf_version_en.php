<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-pdf_version
// Langue: en
// Date: 08-06-2016 16:58:45
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'pdf_version_description' => 'Manage PDF version of website articles.',
	'pdf_version_slogan' => 'PDF Version of contents',
);
?>