<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'icone_voir_pdf_version' => 'View PDF version',
	'titre_configurer' => 'PDF version configuration',
	'label_methode' => 'PDF generation method',
	'label_methode_exec' => '<tt>exec(\'wkhtmltopdf\')</tt> on this server',
	'label_methode_http' => 'external HTTP API call of wkhtmltopdf',
	'label_methode_pdfshift' => 'with <a href="https://pdfshift.io/">PDFShift</a> service',
	'legend_config_exec' => 'Execution on the server',
	'explication_installer_wkhtmltopdf' => 'Instal wkhtmltopdf on the server, after downloading from <a href="http://wkhtmltopdf.org/">http://wkhtmltopdf.org/</a>.',
	'label_wkhtmltopdf_path' => 'wkhtmltopdf binary path',
	'explication_wkhtmltopdf_path' => 'You can fill with several binaries (one per line) that will be used in this order in case of failure',
	'label_api_wkhtmltopdf_actif_1' => 'Open API http for other websites',
	'label_api_keys' => 'Valid keys for API access (one key per line)',
	'explication_api_keys' => 'API is availaible at URL :<br /><tt>@url@[key]</tt>',
	'legend_config_http' => 'HTTP API call on another server',
	'label_wkhtmltopdf_api_url' => 'API URL',
	'legend_config_pdfshift' => 'PDFShift',
	'label_pdfshift_api_key' => 'API Key',
	'label_pdfshift_sandbox_oui' => 'Use in sandbox mode (no credit counted)',

);

?>
