<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'icone_voir_pdf_version' => 'Voir la version PDF',
	'titre_configurer' => 'Configurer la version PDF',
	'label_methode' => 'Méthode de génération',
	'label_methode_exec' => '<tt>exec(\'wkhtmltopdf\')</tt> sur le serveur',
	'label_methode_http' => 'appel de wkhtmltopdf par une API http',
	'label_methode_pdfshift' => 'via le service <a href="https://pdfshift.io/">PDFShift</a>',
	'label_methode_docraptor' => 'via le service <a href="https://docraptor.com/">DocRaptor (Prince XML)</a>',
	'legend_config_exec' => 'Execution sur le serveur',
	'explication_installer_wkhtmltopdf' => 'Installez wkhtmltopdf sur le serveur, après téléchargement depuis <a href="http://wkhtmltopdf.org/">http://wkhtmltopdf.org/</a>.',
	'label_wkhtmltopdf_path' => 'Chemin d\'accès au binaire wkhtmltopdf',
	'explication_wkhtmltopdf_path' => 'Vous pouvez indiquer plusieurs executables (un par ligne) qui seront utilisés les un apres les autres en cas d\'echec',
	'label_api_wkhtmltopdf_actif_1' => 'Ouvrir l\'API http pour d\'autres sites',
	'label_api_keys' => 'Clés valides pour accèder à l\'API (une clé par ligne)',
	'explication_api_keys' => 'L\'API sera accessible par l\'URL :<br /><tt>@url@[clé]</tt>',
	'legend_config_http' => 'Appel par API http sur un autre serveur',
	'label_wkhtmltopdf_api_url' => 'URL de l\'API',
	'legend_config_pdfshift' => 'PDFShift',
	'label_pdfshift_api_key' => 'Clé de l\'API',
	'label_pdfshift_sandbox_oui' => 'Utiliser en mode bac à sable (sans décompter de crédit)',
	'legend_config_docraptor' => 'DocRaptor (Prince XML)',
	'label_docraptor_api_key' => 'Clé de l\'API',
	'label_docraptor_sandbox_oui' => 'Utiliser en mode bac à sable (sans décompter de crédit)',

);

?>
