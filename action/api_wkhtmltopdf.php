<?php

/**
 * Plugin PDF_VERSION v3
 * Licence GPL (c) 2016-2024 Cedric
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

include_spip('inc/pdf_version');

/**
 * API WKHTMLTOPDF :
 * http://www.example.org/wkhtmltopdf.api/abcdef0123456789?url=http://blog.nursit.net
 *
 * toutes les options de wkhtmltopdf peuvent etre passees en GET ou POST
 * l'ordre des options de l'appel sera respecte
 * le html source peut etre une url fournie par url=
 * ou du HTML brut fourni dans html=
 * dans ce dernier cas on preferera appeler l'API en POST pour ne pas etre limite par la longueur du GET
 *
 */
function action_api_wkhtmltopdf_dist() {

	if (!function_exists('lire_config')) {
		include_spip('inc/config');
	}

	// verifier que la fonction API wkhtmltopdf a bien ete activee
	if (
		lire_config('pdf_version/methode', 'exec') !== 'exec'
		or !lire_config('pdf_version/api_wkhtmltopdf_actif', '1')
	) {
		pdf_version_afficher_erreur_document(403);
		exit;
	}
	// recuperer le token et le verifier
	$arg = _request('arg');
	$ok = false;
	$cles = lire_config('pdf_version/api_keys', '');
	if ($cles) {
		$cles = explode("\n", $cles);
		$cles = array_filter($cles);
		foreach ($cles as $cle) {
			$cle = explode(';', $cle);
			$cle = trim(reset($cle));
			if ($arg === $cle) {
				$ok = true;
				break;
			}
		}
	}
	if (!$ok) {
		pdf_version_afficher_erreur_document(403);
		exit;
	}


	$args = [];

	// recuperer les arguments : on utilise $_REQUEST car l'API peut etre appelee en POST ou GET
	foreach ($_REQUEST as $k => $v) {
		if (
			strncmp($k, '--', 2) == 0
			and preg_match(',^--[\w-]+$,i', $k)
		) {
			$args[$k] = $v;
		}
	}

	$tmp_dir = sous_repertoire(_DIR_TMP, 'wkhtmltopdf');
	include_spip('inc/acces');

	$tmp_file = $tmp_dir . substr(md5(creer_uniqid()), 0, 16);
	$pdf_file = $tmp_file . '.pdf';
	$tmp_file_header = $tmp_file_footer = '';

	// les header/footer HTML dans un fichier temporaire
	if (isset($args['--header-html'])) {
		$tmp_file_header = $tmp_file . '-header.html';
		ecrire_fichier($tmp_file_header, $args['--header-html']);
		$args['--header-html'] = $tmp_file_header;
	}

	if (isset($args['--footer-html'])) {
		$tmp_file_footer = $tmp_file . '-footer.html';
		ecrire_fichier($tmp_file_footer, $args['--footer-html']);
		$args['--footer-html'] = $tmp_file_footer;
	}
	$tmp_file .= '.html';

	// le HTML source principal : une URL ou du HTML
	$html_file = '';
	if (_request('url')) {
		$html_file = _request('url');
	}
	elseif (_request('html')) {
		ecrire_fichier($tmp_file, _request('html'));
		$html_file = $tmp_file;
	}

	if ($html_file) {
		$exec_wkhtmltopdf = pdf_version_charger_service();
		$res = $exec_wkhtmltopdf($html_file, $pdf_file, $args);

		if (!$res and defined('_PDF_VERSION_DEBUG')) {
			$tmp_dir_failed = sous_repertoire($tmp_dir, 'fail');
			@rename($tmp_file, $tmp_dir_failed . basename($tmp_file));
			if ($tmp_file_footer) {
				@rename($tmp_file_footer, $tmp_dir_failed . basename($tmp_file_footer));
			}
			if ($tmp_file_header) {
				@rename($tmp_file_header, $tmp_dir_failed . basename($tmp_file_header));
			}
		}
	}


	if (file_exists($pdf_file)) {
		// delivrer le PDF
		$embed = true;

		// toujours envoyer un content type
		header('Content-Type: application/pdf');
		// pour les images ne pas passer en attachment
		// sinon, lorsqu'on pointe directement sur leur adresse,
		// le navigateur les downloade au lieu de les afficher
		if (!$embed) {
			$f = basename($pdf_file);
			header("Content-Disposition: attachment; filename=\"$f\";");
			header('Content-Transfer-Encoding: binary');

			// fix for IE catching or PHP bug issue
			header('Pragma: public');
			header('Expires: 0'); // set expiration time
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		} else {
			header('Expires: 3600'); // set expiration time
		}

		if ($size = filesize($pdf_file)) {
			header('Content-Length: ' . $size);
		}

		readfile($pdf_file);
		@unlink($pdf_file);
	}
	else {
		pdf_version_afficher_erreur_document(404);
	}

	if ($tmp_file and file_exists($tmp_file)) {
		@unlink($tmp_file);
	}
	if ($tmp_file_header and file_exists($tmp_file_header)) {
		@unlink($tmp_file_header);
	}
	if ($tmp_file_footer and file_exists($tmp_file_footer)) {
		@unlink($tmp_file_footer);
	}
}
