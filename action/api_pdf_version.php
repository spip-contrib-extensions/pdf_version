<?php

/**
 * Plugin PDF_VERSION v3
 * Licence GPL (c) 2016-2024 Cedric
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

include_spip('inc/pdf_version');

function action_api_pdf_version_dist($arg = null, $embed = true) {

	if (is_null($arg)) {
		$arg = _request('arg');
	}

	// verifier que arg est bien forme
	if (!$arg) {
		pdf_version_afficher_erreur_document(403);
		exit;
	}

	$quoi = 'objet';
	if (strpos($arg, '/') !== false) {
		$arg = explode('/', $arg, 2);
		$quoi = array_shift($arg);
		$arg = end($arg);
	}

	if (!function_exists($f = 'action_api_pdf_version_' . $quoi)) {
		spip_log("Fonction $f inconnue pour generer une version pdf avec $quoi/$arg", 'pdf_version' . _LOG_ERREUR);
		pdf_version_afficher_erreur_document(403);
		exit;
	}

	$contexte = [];
	foreach ($_REQUEST as $k => $v) {
		if (!in_array($k, ['action', 'arg']) and strpos($k, 'var_') !== 0) {
			$contexte[$k] = $v;
		}
	}

	$pdf_file = $f($arg, $contexte);

	// delivrer le PDF
	// Vider tous les tampons pour ne pas provoquer de Fatal memory exhausted
	$level = @ob_get_level();
	while ($level--) {
		@ob_end_clean();
	}

	// toujours envoyer un content type
	header('Content-Type: application/pdf');
	// pour les images ne pas passer en attachment
	// sinon, lorsqu'on pointe directement sur leur adresse,
	// le navigateur les downloade au lieu de les afficher
	$filename = basename($pdf_file);
	if (!$embed) {
		header("Content-Disposition: attachment; filename=\"$filename\";");
		header('Content-Transfer-Encoding: binary');

		// fix for IE catching or PHP bug issue
		header('Pragma: public');
		header('Expires: 0'); // set expiration time
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	} else {
		header("Content-Disposition: inline; filename=\"$filename\";");
		header('Expires: 3600'); // set expiration time
	}

	if ($size = filesize($pdf_file)) {
		header('Content-Length: ' . $size);
	}

	readfile($pdf_file);
}

function action_api_pdf_version_objet($arg, $contexte) {

	$filename_base = basename($arg, '.pdf');

	if (strpos($filename_base, '--') !== false) {
		$args = explode('--', $filename_base);
		$id = array_pop($args);
		$args = implode('--', $args);
		$args = explode('-', $args);
	}
	else {
		$args = explode('-', $filename_base);
		$id = array_pop($args);
	}
	$objet = array_shift($args);

	$composition = '';
	if (count($args)) {
		$composition = implode('-', $args);
	}

	// si pas de - ou - en premiere position, c'est un id_objet
	if (! strpos($id, '-')) {
		$id_objet = intval($id);
	}
	// on peut passer une cle dans l'URL pour utiliser autre chose que la cle primaire:
	// pdf_version.api/objet/{$objet}-{$champ_cle}={$valeur_cle}.pdf
	else {
		list($champ_cle, $valeur_cle) = explode('-', $id, 2);

		$champ_cle = preg_replace(',\W,', '_', $champ_cle);
		$valeur_cle = trim(strval($valeur_cle));

		$primary = id_table_objet($objet);
		$table_sql = table_objet_sql($objet);
		$id_objet = sql_getfetsel($primary, $table_sql, "$champ_cle=" . sql_quote($valeur_cle));
	}



	if (
		preg_match(',\W,', $objet)
		or !is_numeric($id_objet)
		or !$id_objet = intval($id_objet)
		or ($composition and !preg_match(',^[\w-]+$,', $composition))
	) {
		spip_log("action_api_pdf_version_objet: Syntaxe incorrecte $arg ($objet / $id_objet / $composition)", 'pdf_version' . _LOG_ERREUR);
		pdf_version_afficher_erreur_document(403);
		exit;
	}

	include_spip('inc/autoriser');

	// verifier le droit de voir cette version PDF
	if (!autoriser('voirpdfversion', $objet, $id_objet, null, ['composition' => $composition])) {
		pdf_version_afficher_erreur_document(403);
		exit;
	}

	// si le fichier n'existe pas, le generer a la volee
	// via la fonction adhoc ou generique
	$pdf_file = pdf_version_dir() . $filename_base . '.pdf';

	$recalcul = true;
	$var_mode_recalcul = pdf_version_recalculer();
	if (file_exists($pdf_file)) {
		$recalcul = $var_mode_recalcul;
		if ($pdf_version_determine_pdf_valide = charger_fonction('pdf_version_determine_pdf_valide', 'inc', true)) {
			if (!$pdf_version_determine_pdf_valide('objet', ['objet' => $objet, 'id_objet' => $id_objet, 'composition' => $composition], $pdf_file)) {
				$recalcul = true;
			}
		}
	}

	if ($recalcul) {
		generer_pdf_version_objet($objet . ($composition ? "-{$composition}" : ''), $id_objet, $pdf_file, $var_mode_recalcul);

		// securite
		if (!file_exists($pdf_file)) {
			spip_log("Echec generation PDF $objet $id_objet" . ($composition ? " composition $composition" : ''), 'pdf_version' . _LOG_ERREUR);
			pdf_version_afficher_erreur_document(404);
			exit;
		}
	}

	return $pdf_file;
}


function action_api_pdf_version_page($arg, $contexte) {
	$arg = basename($arg, '.pdf');
	$page = $arg;

	if (preg_match(',\[^\w-],', $page)) {
		spip_log("action_api_pdf_version_page: Syntaxe incorrecte $arg ($page)", 'pdf_version' . _LOG_ERREUR);
		pdf_version_afficher_erreur_document(403);
		exit;
	}

	include_spip('inc/autoriser');

	// verifier le droit de voir cette version PDF
	if (!autoriser('voirpdfversionpage', $page, null, null, $contexte)) {
		pdf_version_afficher_erreur_document(403);
		exit;
	}

	// si le fichier n'existe pas, le generer a la volee
	// via la fonction adhoc ou generique
	$hash = '';
	if ($contexte) {
		ksort($contexte);
		$hash = substr(md5(json_encode($contexte)), 0, 8);
		$hash = "-$hash";
	}
	$pdf_file = pdf_version_dir() . 'page-' . $arg . $hash . '.pdf';

	$recalcul = true;
	$var_mode_recalcul = pdf_version_recalculer();
	if (file_exists($pdf_file)) {
		$recalcul = $var_mode_recalcul;
		if ($pdf_version_determine_pdf_valide = charger_fonction('pdf_version_determine_pdf_valide', 'inc', true)) {
			if (!$pdf_version_determine_pdf_valide('page', array_merge($contexte, ['page' => $page]), $pdf_file)) {
				$recalcul = true;
			}
		}
	}

	if ($recalcul) {
		generer_pdf_version_page($page, $contexte, $pdf_file, $var_mode_recalcul);

		// securite
		if (!file_exists($pdf_file)) {
			spip_log("Echec generation PDF Page $page " . json_encode($contexte), 'pdf_version' . _LOG_ERREUR);
			pdf_version_afficher_erreur_document(404);
			exit;
		}
	}

	return $pdf_file;
}


function action_api_pdf_version_merge($arg, $contexte) {
	$arg = basename($arg, '.pdf');
	$liste = $arg;

	if (preg_match(',\[^\w-],', $liste)) {
		spip_log("action_api_pdf_version_merge: Syntaxe incorrecte $arg ($liste)", 'pdf_version' . _LOG_ERREUR);
		pdf_version_afficher_erreur_document(403);
		exit;
	}

	include_spip('inc/autoriser');

	// verifier le droit de voir cette version PDF
	if (!autoriser('voirpdfversionmerge', $liste)) {
		pdf_version_afficher_erreur_document(403);
		exit;
	}

	// si le fichier n'existe pas, le generer a la volee
	// via la fonction adhoc ou generique
	$hash = '';
	if ($contexte) {
		ksort($contexte);
		$hash = substr(md5(json_encode($contexte)), 0, 8);
		$hash = "-$hash";
	}
	$pdf_file = pdf_version_dir() . 'merge-' . $arg . $hash . '.pdf';

	$recalcul = true;
	$var_mode_recalcul = pdf_version_recalculer();
	if (file_exists($pdf_file)) {
		$recalcul = $var_mode_recalcul;
		if ($pdf_version_determine_pdf_valide = charger_fonction('pdf_version_determine_pdf_valide', 'inc', true)) {
			if (!$pdf_version_determine_pdf_valide('merge', array_merge($contexte, ['liste' => $liste]), $pdf_file)) {
				$recalcul = true;
			}
		}
	}

	if ($recalcul) {
		generer_pdf_version_merge($liste, $contexte, $pdf_file, $var_mode_recalcul);

		// securite
		if (!file_exists($pdf_file)) {
			spip_log("Echec generation PDF Merge $liste " . json_encode($contexte), 'pdf_version' . _LOG_ERREUR);
			pdf_version_afficher_erreur_document(404);
			exit;
		}
	}

	return $pdf_file;
}

/**
 * Tester si le mode recalcul est présent dans l'URL
 *
 * @note Il peux y avoir plusieurs items : `var_mode=recalcul,preview`
 * Dans ce cas _VAR_MODE retourne un seul item, et _request() la valeur brute.
 * _VAR_MODE n'est pas systématiquement définie.
 *
 * @return bool
 */
function pdf_version_recalculer(): bool {
	$var_mode = (defined('_VAR_MODE') ? _VAR_MODE : _request('var_mode'));
	return in_array('recalcul', explode(',', $var_mode));
}
