<?php

/**
 * Plugin PDF_VERSION v3
 * Licence GPL (c) 2016-2024 Cedric
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}


/**
 * Appel de wkhtmltopdf par API sur un autre site SPIP
 * @param string $html_file
 * @param string $pdf_file
 * @param array $args
 */
function pdf_version_services_wkhtmltopdf_http_dist($html_file, $pdf_file, $args = []) {

	include_spip('inc/config');
	$logfile = 'pdfv_wkhtmltopdf_http';

	if ($wkhtmltopdf_api_url = lire_config('pdf_version/wkhtmltopdf_api_url')) {
		// Appel de wkhtmltopdf via API http

		// lire le html et le renseigner dans les arguments
		$html = '';
		lire_fichier($html_file, $html);
		$args['html'] = wkhtmltopdf_normalise_retours_ligne_html($html);

		$vars = [
			'@page@' => '[page]',
			'@total_page@' => '[topage]',
		];

		if (isset($args['--header-html'])) {
			lire_fichier($args['--header-html'], $args['--header-html']);
			$args['--header-html'] = pdf_version_renomme_variables($vars, $args['--header-html']);
			$args['--header-html'] = wkhtmltopdf_normalise_retours_ligne_html($args['--header-html']);
		}
		if (isset($args['--footer-html'])) {
			lire_fichier($args['--footer-html'], $args['--footer-html']);
			$args['--footer-html'] = pdf_version_renomme_variables($vars, $args['--footer-html']);
			$args['--footer-html'] = wkhtmltopdf_normalise_retours_ligne_html($args['--footer-html']);
		}

		$result = true;
		include_spip('inc/distant');
		$res = recuperer_url($wkhtmltopdf_api_url, [
			'methode' => 'POST',
			'datas' => $args,
			'file' => "{$pdf_file}.tmp",
		]);
		if (!$res or $res['status'] > 300) {
			@unlink("{$pdf_file}.tmp");
			$result = false;
		}

		if ($result) {
			$extract = file_get_contents("{$pdf_file}.tmp", false, null, 0, 16000);
			$extract = ltrim($extract);
			if (
				stripos($extract, '<!DOCTYPE html>') === 0
				and stripos($extract, '<html') !== false
				and stripos($extract, 'minipres') !== false
			) {
				// c'est visiblement une minipres html, donc le serveur a foire et on a pas reconnus le status code :(
				@unlink("{$pdf_file}.tmp");
				$result = false;

				// renseigner le status pour les logs au lieu du 200
				if (isset($res['status'])) {
					if (preg_match(',<title>(.*)</title>,Uims', $extract, $m)) {
						$res['status'] .= ' / ' . $m[1];
					}
				}
			}
		}
		if ($result) {
			@rename("{$pdf_file}.tmp", $pdf_file);
		}
		else {
			@unlink("{$pdf_file}.tmp");
		}

		spip_log($wkhtmltopdf_api_url . " $html_file -> $pdf_file : " . var_export($res, true), $logfile . ($result ? '' : _LOG_ERREUR));

		return $result;
	}

	spip_log('service wkhtmltopdf_http mal configure', $logfile . _LOG_ERREUR);
	return false;
}


function &wkhtmltopdf_normalise_retours_ligne_html(&$texte) {
	$texte = str_replace("\r\n", "\n", $texte);
	$texte = str_replace("\r", "\n", $texte);
	$texte = str_replace("\n", "\r\n", $texte);

	return $texte;
}
