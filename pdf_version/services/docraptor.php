<?php

/**
 * Plugin PDF_VERSION v3
 * Licence GPL (c) 2016-2024 Cedric
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Appel de DocRaptor (Prince XML) par API
 * cf https://docraptor.com/documentation
 *
 * @param string $html_file
 * @param string $pdf_file
 * @param array $args
 */
function pdf_version_services_docraptor_dist($html_file, $pdf_file, $args = []) {

	include_spip('inc/config');
	$logfile = 'pdfv_docraptor';

	if ($docraptor_api_key = lire_config('pdf_version/docraptor_api_key')) {
		$javascript_engine = (defined('_PDF_VERSION_DOCRAPTOR_JAVASCRIPT_ENGINE') ? _PDF_VERSION_DOCRAPTOR_JAVASCRIPT_ENGINE : 'browser');
		if (!in_array($javascript_engine, ['browser', 'princexml', 'none'])) {
			$javascript_engine = 'browser';
		}
		$docraptor_options = [
			'document_content' => '(...)',
			'type' => 'pdf',
			// javascript options
			'javascript' => $javascript_engine === 'browser', // browser javascript engine
			'ignore_console_messages' => true,
			'prince_options' => [
				'media' => 'screen', // use screen styles instead of print styles
				'baseurl' => $GLOBALS['meta']['adresse_site'], // pretend URL when using document_content
				'css_dpi' => $args['--dpi'],
				'javascript' => $javascript_engine = 'princexml', // prince-xml javascript engine
			],
		];

		if (lire_config('pdf_version/docraptor_sandbox')) {
			$docraptor_options['test'] = true;
		}

		$page_size = 'size: ' . $args['--page-size'] ?? 'A4';
		$landscape = empty($args['--orientation']) ? false : ($args['--orientation'] === 'Portrait' ? false : true);
		$page_size .= ($landscape ? ' landscape' : '') . ";\n";

		$extra_html = '';
		$extra_css = "@page {\n"
			. $page_size
			. 'margin:' . implode(' ', [$args['--margin-top'], $args['--margin-right'], $args['--margin-bottom'], $args['--margin-left']]) . ";\n"
			. '}';


		spip_log('docraptor: ' . " $html_file -> $pdf_file : options=" . json_encode($docraptor_options), $logfile . _LOG_DEBUG);

		$vars = [
			'@page@' => '<span class="docraptor-page-number"></span>',
			'@total_page@' => '<span class="docraptor-page-total"></span>',
		];
		$extra_css .= '.docraptor-page-number{content:counter(page);}.docraptor-page-total{content:counter(pages);}';

		if (isset($args['--header-html'])) {
			lire_fichier($args['--header-html'], $html);
			$html = pdf_version_renomme_variables($vars, $html);

			if (preg_match(',<body[^>]*>(.*)</body>,Uims', $html, $match)) {
				if ($header_html = trim($match[1])) {
					$extra_html .= '<div class="docraptor-header-center-html">' . $header_html . '</div>';
					$extra_css .= '@page { @top-center { content: element(header-top-html);}} .docraptor-header-center-html{position:running(header-top-html);}';
				}
			}
		}
		if (isset($args['--footer-html'])) {
			lire_fichier($args['--footer-html'], $html);
			$html = pdf_version_renomme_variables($vars, $html);

			if (preg_match(',<body[^>]*>(.*)</body>,Uims', $html, $match)) {
				if ($footer_html = trim($match[1])) {
					$extra_html .= '<div class="docraptor-footer-center-html">' . $footer_html . '</div>';
					$extra_css .= '@page { @bottom-center { content: element(footer-bottom-html);}} .docraptor-footer-center-html{position:running(footer-bottom-html);}';
				}
			}
		}

		// lire le html et ajouter extra css et html
		$html = '';
		lire_fichier($html_file, $html);

		if ($extra_css and $p = strpos($html, '</head>')) {
			$extra_css = "<style type='text/css'>$extra_css</style>";
			$html = substr_replace($html, $extra_css, $p, 0);
		}
		if ($extra_html and $p = strpos($html, '<body')) {
			$pclose = strpos($html, '>', $p) + 1;
			$html = substr_replace($html, $extra_html, $pclose, 0);
		}
		//var_dump($html);die();

		$docraptor_options['document_content'] = $html;
		$tmp_pdf_file = $pdf_file . '.tmp';
		$res = docraptor_api_call('docs', $docraptor_api_key, $docraptor_options, $tmp_pdf_file);

		if (
			!empty($res)
			and $res['length']
			and $res['status'] == 200
			and file_exists($tmp_pdf_file)
		) {
			@rename($tmp_pdf_file, $pdf_file);
			return true;
		}

		@unlink($tmp_pdf_file);
		spip_log("docraptor: Echec generation fichier $pdf_file", $logfile . _LOG_ERREUR);
		return false;
	}


	spip_log('service docraptor mal configure', $logfile . _LOG_ERREUR);
	return false;
}

function docraptor_api_call($path, $api_key, $data, $output_file) {
	$logfile = 'pdfv_docraptor';
	$url_endpoint = 'https://api.docraptor.com/';
	$url_api = $url_endpoint . ltrim($path, '/');

	defined('_INC_DISTANT_CONNECT_TIMEOUT') || define('_INC_DISTANT_CONNECT_TIMEOUT', 30);

	include_spip('inc/distant');
	$options = [
		'headers' => [
			// api key as the user, blank password
			'Authorization' => 'Basic ' . base64_encode("$api_key:"),
		],
		'file' => $output_file,
	];

	if (!empty($data)) {
		$options['methode'] = 'POST';
		$options['datas'] = "Content-Type: application/json\r\n\r\n" . json_encode($data);
	}

	$res = recuperer_url($url_api, $options);
	if (empty($res)) {
		spip_log("docraptor_api_call: $url_api resultat innatendu " . json_encode($res), $logfile . _LOG_ERREUR);
		return false;
	}

	spip_log("docraptor_api_call: $url_api resultat " . json_encode($res), $logfile . _LOG_DEBUG);

	return $res;
}
