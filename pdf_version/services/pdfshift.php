<?php

/**
 * Plugin PDF_VERSION v3
 * Licence GPL (c) 2016-2024 Cedric
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}


/**
 * Appel de PDFShift par API
 * @param string $html_file
 * @param string $pdf_file
 * @param array $args
 */
function pdf_version_services_pdfshift_dist($html_file, $pdf_file, $args = []) {

	include_spip('inc/config');
	$logfile = 'pdfv_pdfshift';

	if ($pdfshift_api_key = lire_config('pdf_version/pdfshift_api_key')) {
		$pdfshift_api_url = 'https://api.pdfshift.io/v3/convert/pdf';

		// Appel de pdfshift via API http

		$html = '';
		$pdfshift_options = [
			'source' => '(...)',
			'use_print' => false,
			'delay' => 500,
			'format' => $args['--page-size'] ?? 'A4',
			'landscape' => empty($args['--orientation']) ? false : ($args['--orientation'] === 'Portrait' ? false : true),
			'margin' => implode(' ', [$args['--margin-top'], $args['--margin-right'], $args['--margin-bottom'], $args['--margin-left']]),
		];

		// margin et format semblent pas vraiment pris en compte, on doublonne avec une directive css
		$pdfshift_options['css'] = '@page{size:' . $pdfshift_options['format'] . ';margin:' . $pdfshift_options['margin'] . '}';

		// todo : substitution des variables dans le header et footer
		/*
			Variable 	Description
			{{date}} 	Formatted print date
			{{title}} 	Title of the HTML document
			{{url}} 	Page URL
			{{page}} 	Current page
			{{total}} 	Total number of pages
		*/
		$vars = [
			'@page@' => '{{page}}',
			'@total_page@' => '{{total}}',
		];

		if (isset($args['--header-html'])) {
			lire_fichier($args['--header-html'], $html);
			$html = pdf_version_renomme_variables($vars, $html);

			// il faut reinserer les margin left/right du document dans le footer car il est par defaut sans margin
			$mleft = $args['--margin-left'];
			$mright = $args['--margin-right'];
			$style_add = "<style>#header {margin-left:calc(0.75 * {$mleft});margin-right:calc(0.75 * {$mright});}</style>";
			if ($p = strpos($html, '</head>')) {
				$html = substr_replace($html, $style_add, $p, 0);
			}
			else {
				$html .= $style_add;
			}
			$pdfshift_options['header'] = ['source' => $html, 'height' => $args['--header-spacing'] ?? '1px'];
			if (is_numeric($pdfshift_options['header']['height'])) {
				$pdfshift_options['header']['height'] .= 'px';
			}
		}
		if (isset($args['--footer-html'])) {
			lire_fichier($args['--footer-html'], $html);
			$html = pdf_version_renomme_variables($vars, $html);

			// il faut reinserer les margin left/right du document dans le footer car il est par defaut sans margin
			$mleft = $args['--margin-left'];
			$mright = $args['--margin-right'];
			$mbottom = $args['--margin-bottom'];
			$style_add = "<style>#footer {margin-left:calc(0.75 * {$mleft});margin-right:calc(0.75 * {$mright});margin-bottom: calc(0.5 * $mbottom)}</style>";
			if ($p = strpos($html, '</head>')) {
				$html = substr_replace($html, $style_add, $p, 0);
			}
			else {
				$html .= $style_add;
			}
			$pdfshift_options['footer'] = ['source' => $html, 'height' => $args['--footer-spacing'] ?? '1px'];
			if (is_numeric($pdfshift_options['footer']['height'])) {
				$pdfshift_options['footer']['height'] .= 'px';
			}
		}

		if (lire_config('pdf_version/pdfshift_sandbox')) {
			$pdfshift_options['sandbox'] = true;
		}

		spip_log($pdfshift_api_url . " $html_file -> $pdf_file : options=" . json_encode($pdfshift_options), $logfile . _LOG_DEBUG);

		// lire le html et le renseigner dans les arguments
		$html = '';
		lire_fichier($html_file, $html);
		$pdfshift_options['source'] = $html;


		// un watermark ?
		$watermarks = [
			'pdf' => [
				'--pdfshit-watermark-source'
			],
			'texte' => [
				'--pdfshit-watermark-text',
				'--pdfshit-watermark-font_size',
				'--pdfshit-watermark-font_family',
				'--pdfshit-watermark-font_color',
				'--pdfshit-watermark-font_opacity',
				'--pdfshit-watermark-font_bold',
				'--pdfshit-watermark-font_italic',
				'--pdfshit-watermark-offset_x',
				'--pdfshit-watermark-offset_y',
				'--pdfshit-watermark-rotate'
			],
			'image' => [
				'--pdfshit-watermark-image',
				'--pdfshit-watermark-offset_x',
				'--pdfshit-watermark-offset_y',
				'--pdfshit-watermark-rotate'
			],
		];
		foreach ($watermarks as $type => $vars) {
			// si on a un --pdfshit-watermark-source en base64 dans le source c'est mieux de le nettoyer
			$watermark = pdf_version_collecter_variables_head($vars, $pdfshift_options['source'], $type === 'pdf' ? true : false);
			$first_var = reset($vars);
			if (!empty($watermark[$first_var])) {
				$pdfshift_options['watermark'] = [];
				foreach ($watermark as $k => $v) {
					$k = substr($k, strlen('--pdfshit-watermark-'));
					$pdfshift_options['watermark'][$k] = $v;
				}
				break;
			}
		}

		$outline_titles = pdfshift_outline_hn($pdfshift_options['source']);
		$result = true;
		$curl = curl_init();
		curl_setopt_array($curl, [
			CURLOPT_URL => $pdfshift_api_url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => json_encode($pdfshift_options),
			CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
			CURLOPT_USERPWD => "api:$pdfshift_api_key"
		]);

		$response = curl_exec($curl);
		$res = curl_getinfo($curl);
		$trace = json_encode($res);

		if (
			!empty($res['http_code'])
			and $res['http_code'] === 200
			and $response !== false
		) {
			//file_put_contents("{$pdf_file}.ori", $response);
			pdfshift_postraite_outline($response, $outline_titles);
			file_put_contents("{$pdf_file}.tmp", $response);
		}
		else {
			$result = false;
			// en cas d'erreur pdfshift renvoie un json avec le code d'erreur et le message, on le loge donc
			if ($response !== false and strlen($response)) {
				$trace .= ' | ' . $response;
			}
		}

		if ($result) {
			@rename("{$pdf_file}.tmp", $pdf_file);
		}
		else {
			@unlink("{$pdf_file}.tmp");
		}

		spip_log($pdfshift_api_url . " $html_file -> $pdf_file : $trace", $logfile . ($result ? '' : _LOG_ERREUR));

		return $result;
	}

	spip_log('service pdfshift mal configure', $logfile . _LOG_ERREUR);
	return false;
}

/**
 * Ajouter une ancre id devant chaque titre <hn> du html pour générer un outline automatique
 * post génération par pdfshift
 *
 * @param string &$html
 * @return array
 */
function pdfshift_outline_hn(&$html) {

	$html_outline = '';
	$titles = [];
	if (preg_match_all(",<h([123456])\b[^>]*>(.*?)</h\\1>,ims", $html, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE)) {
		foreach ($matches as $m) {
			$title = [
				'pos' => $m[0][1],
				'niveau' => $m[1][0],
				'texte' => textebrut($m[2][0]),
				'raw' => $m[0][0],
			];
			$title['anchor'] = 'tit' . $title['niveau'] . '-' . substr(md5($title['pos'] . ':' . $title['raw']), 0, 8);
			$titles[] = $title;
		}

		foreach (array_reverse($titles) as $t) {
			$anchor = $t['anchor'];
			$html = substr_replace($html, "<span id=\"$anchor\"></span>", $t['pos'], 0);
			$html_outline = "<li><a href=\"#$anchor\">" . $t['texte'] . "</a></li>\n" . $html_outline;
		}

		// pour que les ancres restents dans le pdf il faut un lien qui pointe dessus
		// on génère donc un plan en fin de document, mais non visible
		$html_outline = "<div class='outline'"
			. " style='position: absolute;left:0;bottom:0;width: 1px;height: 1px;font-size: 1px;overflow: hidden'"
			. "><ul>$html_outline</ul></div>";
		$html = str_replace('</body>', $html_outline . '</body>', $html);
	}

	#spip_log(json_encode($titles), 'dbgoutline');

	return $titles;
}

function pdfshift_postraite_outline(&$pdf_source, $titles) {
	if (empty($titles)) {
		return;
	}

	$infos = pdfshift_parser_get_xref_infos($pdf_source);
	$nb_obj = $infos['xref']['nb_obj'];

	$outline_declaration = null;
	$i = 1;
	while ($i < $nb_obj) {
		$object = pdfshift_parser_get_raw_object($infos, $pdf_source, $i);
		#spip_log("OUTLINE declaration: lire objet $i OK", 'dbgoutline');
		if (($p = strpos($object['command'], '/Outlines '))
		 && ($p = strpos($object['raw'], '/Outlines '))
		 && preg_match(',/Outlines (\d+).*?\n,', $object['command'], $m)) {
			#spip_log("OUTLINE declaration: Outlines trouvé dans objet $i", 'dbgoutline');
			$outline_declaration = [
				'object' => $object,
				'pos' => $p,
				'root' => $m[1],
				'length' => strlen($m[0]),
				'raw_line' => $m[0]
			];
			break;
		}
		$i++;
	}

	#spip_log("OUTLINE declaration:".var_export($outline_declaration, true), 'dbgoutline');

	if (!$outline_declaration) {
		return;
	}

	// Trouver le dernier numero d'objet
	$lastObj = $nb_obj - 1;

	// generer le outline
	$outlines = [];
	foreach ($titles as $title) {
		$outlines[] = [
			'l' => $title['niveau'] - 1,
			't' => $title['texte'],
			'd' => '#' . $title['anchor']
		];
	}

	$bookmarkWriter = new BookmarkWriter($lastObj);
	if ($objects_raw = $bookmarkWriter->generateBookmarks($outlines)) {
		// inserer l'outline d'abord, car c'est un ajout d'objets à la fin
		foreach ($objects_raw as $raw) {
			#spip_log("OUTLINE declaration: ajout objet $raw", 'dbgoutline');
			$object_number = pdfshift_parser_add_object($infos, $pdf_source, $raw);
		}
		// le dernier objet inséré est l'entrée de l'outline, il faut donc modifier l'objet qui contient la déclaration d'outline
		$outlineRoot = $object_number;
		$new_outline = "/Outlines $outlineRoot 0 R\n/PageMode /UseOutlines\n";
		$new_raw = str_replace($outline_declaration['raw_line'], $new_outline, $outline_declaration['object']['raw']);

		#spip_log("OUTLINE declaration: nouvelle $new_raw", 'dbgoutline');
		pdfshift_parser_replace_raw_object($infos, $pdf_source, $outline_declaration['object']['index'], $new_raw);
	}
}

class BookmarkWriter
{
	protected array $outlines = [];

	protected int $objectNumber = 0;

	protected string $buffer = '';

	public function __construct($lastObj) {
		$this->objectNumber = $lastObj;
	}

	protected function write($s, $ln = true) {
		$this->buffer .= $s . ($ln ? "\n" : '');
	}

	public function getBuffer() {
		return $this->buffer;
	}

	public function utf16BigEndianTextString($s) {
        // _UTF16BEtextstring
		$s = $this->utf8ToUtf16BigEndian($s, true);

		return '(' . $this->escape($s) . ')';
	}

	// Converts UTF-8 strings to UTF16-BE.
	public function utf8ToUtf16BigEndian($str, $setbom = true) {
        // UTF8ToUTF16BE
		/*
		if ($this->mpdf->checkSIP && preg_match("/([\x{20000}-\x{2FFFF}])/u", $str)) {
			if (!in_array($this->mpdf->currentfontfamily, ['gb', 'big5', 'sjis', 'uhc', 'gbB', 'big5B', 'sjisB', 'uhcB', 'gbI', 'big5I', 'sjisI', 'uhcI',
				'gbBI', 'big5BI', 'sjisBI', 'uhcBI'])) {
				$str = preg_replace("/[\x{20000}-\x{2FFFF}]/u", chr(0), $str);
			}
		}
		*/
		if (/*$this->mpdf->checkSMP &&*/ preg_match("/([\x{10000}-\x{1FFFF}])/u", $str)) {
			$str = preg_replace("/[\x{10000}-\x{1FFFF}]/u", chr(0), $str);
		}

		$outstr = ''; // string to be returned
		if ($setbom) {
			$outstr .= "\xFE\xFF"; // Byte Order Mark (BOM)
		}

		$outstr .= mb_convert_encoding($str, 'UTF-16BE', 'UTF-8');

		return $outstr;
	}

	public function escape($s) {
        // _escape
		return strtr($s, [')' => '\\)', '(' => '\\(', '\\' => '\\\\', chr(13) => '\r']);
	}



	public function generateBookmarks($outlines) {
		$nb = count($outlines);
		if ($nb === 0) {
			return 0;
		}


		$bmo = $outlines;
		$this->outlines = [];
		$lastlevel = -1;
		for ($i = 0; $i < count($bmo); $i++) {
			if ($bmo[$i]['l'] > 0) {
				while ($bmo[$i]['l'] - $lastlevel > 1) { // If jump down more than one level, insert a new entry
					$new = $bmo[$i];
					$new['t'] = '[' . $new['t'] . ']'; // Put [] around text/title to highlight
					$new['l'] = $lastlevel + 1;
					$lastlevel++;
					$this->outlines[] = $new;
				}
			}
			$this->outlines[] = $bmo[$i];
			$lastlevel = $bmo[$i]['l'];
		}
		$nb = count($this->outlines);

		$lru = [];
		$level = 0;
		foreach ($this->outlines as $i => $o) {
			if ($o['l'] > 0) {
				$parent = $lru[$o['l'] - 1];
				// Set parent and last pointers
				$this->outlines[$i]['parent'] = $parent;
				$this->outlines[$parent]['last'] = $i;
				if ($o['l'] > $level) {
					// Level increasing: set first pointer
					$this->outlines[$parent]['first'] = $i;
				}
			} else {
				$this->outlines[$i]['parent'] = $nb;
			}
			if ($o['l'] <= $level and $i > 0) {
				// Set prev and next pointers
				$prev = $lru[$o['l']];
				$this->outlines[$prev]['next'] = $i;
				$this->outlines[$i]['prev'] = $prev;
			}
			$lru[$o['l']] = $i;
			$level = $o['l'];
		}

		// Outline items
		$objects_raw = [];
		$n = $this->objectNumber + 1;
		foreach ($this->outlines as $i => $o) {
			$this->buffer = '';
			$this->write('<<');
			$this->write('/Title ' . $this->utf16BigEndianTextString($o['t']));
			$this->write('/Parent ' . ($n + $o['parent']) . ' 0 R');
			if (isset($o['prev'])) {
				$this->write('/Prev ' . ($n + $o['prev']) . ' 0 R');
			}
			if (isset($o['next'])) {
				$this->write('/Next ' . ($n + $o['next']) . ' 0 R');
			}
			if (isset($o['first'])) {
				$this->write('/First ' . ($n + $o['first']) . ' 0 R');
			}
			if (isset($o['last'])) {
				$this->write('/Last ' . ($n + $o['last']) . ' 0 R');
			}

			$anchor = str_replace('#', '\057', $o['d']);
			$this->write(sprintf('/Dest (%s)', $anchor));

			$this->write('/Count 0');
			$this->write('>>');
			$objects_raw[] = $this->buffer;
		}

		// Outline root
		$this->buffer = '';
		$this->write('<<');
		$this->write('/Type /BMoutlines /First ' . $n . ' 0 R');
		$this->write('/Last ' . ($n + $lru[0]) . ' 0 R');
		$this->write('>>');
		$objects_raw[] = $this->buffer;

		return $objects_raw;
	}
}


/*
 * Parsing et manipulation basique d'un PDF, fonctionne pour les PDF générés par PDFShift
 */


function pdfshift_parser_get_xref_infos($content) {

	$details = [
		'raw' => [],
	];

	if (!$p = strrpos($content, 'startxref')) {
		return null;
	}

	$details['startxref'] = intval(substr($content, $p + 9));
	if (empty($details['startxref']) || $details['startxref'] > $p) {
		return null;
	}

	$details['raw']['tail'] = substr($content, $details['startxref']);
	$split = explode('trailer', $details['raw']['tail'], 2);
	$details['raw']['xref'] = array_shift($split);
	$split = explode('startxref', reset($split));
	$details['raw']['trailer'] = $split[0];

	$details['xref'] = [
		'offset' => 0,
		'nb_obj' => 0,
		'lines' => []
	];
	$xref = ltrim($details['raw']['xref']);
	if (!str_starts_with($xref, 'xref')) {
		return null;
	}
	$xref = ltrim(substr($xref, 4));
	$lines = explode("\n", rtrim($xref));
	$first = array_shift($lines);
	$first = explode(" ", $first, 2);
	$details['xref']['offset'] = intval(array_shift($first));
	$details['xref']['nb_obj'] = intval(array_shift($first));
	$details['xref']['lines'] = $lines;


	return $details;
}

function pdfshift_parser_get_raw_object($xref_infos, $pdf_content, $object_number) {
	if ($object_number < 1 || $object_number > $xref_infos['xref']['nb_obj'] - 1) {
		return null;
	}
	$start = intval($xref_infos['xref']['lines'][$object_number]);
	if ($object_number === $xref_infos['xref']['nb_obj'] - 1) {
		$end = $xref_infos['startxref'];
	} else {
		$end = intval($xref_infos['xref']['lines'][$object_number + 1]);
	}

	$object = [
		'index' => $object_number,
		'position' => $start,
		'length' => $end - $start,
		'raw' => substr($pdf_content, $start, $end - $start),
		'command' => '',
		'streams' => [
		],
	];
	$command = $object['raw'];
	// extraire les streams des commandes
	$pstart = $pend = 0;
	while (($p = strpos($command, 'stream', $pstart)) !== false
		&& ($pend = strpos($command, 'endstream', $p)) !== false) {
		$pstart = $p;
		$length = $pend + 9 - $pstart;
		$stream = substr($command, $pstart, $length);
		$md5 = md5($stream);
		$object['streams'][$md5] = $stream;
		$insert = "stream[{$md5}]endstream";
		$command = substr_replace($command, $insert, $pstart, $length);
		$pstart += strlen($insert) - 1;
	}
	$object['command'] = $command;

	return $object;
}

function pdfshift_parser_replace_raw_object(&$xref_infos, &$pdf_content, $object_number, $raw_replace) {
	if ($object = pdfshift_parser_get_raw_object($xref_infos, $pdf_content, $object_number)) {
		$new_length = strlen($raw_replace);
		$position = $object['position'];
		$deltapos = $new_length - $object['length'];
		$pdf_content = substr_replace($pdf_content, $raw_replace, $object['position'], $object['length']);


		if ($deltapos !== 0) {
			// et ensuite il faut remettre a jour toutes les positions qui suivent
			if ($xref_infos['startxref'] > $position) {
				$xref_infos['startxref'] += $deltapos;
			}
			foreach ($xref_infos['xref']['lines'] as $k => $line) {
				$line = explode(' ', $line, 2);
				if (intval($line[0]) > $position) {
					$line[0] = str_pad(intval($line[0]) + $deltapos, strlen($line[0]), 0, STR_PAD_LEFT);
					$xref_infos['xref']['lines'][$k] = implode(' ', $line);
				}
			}
			pdfshift_parser_rewrite_xref_trailer_end($xref_infos, $pdf_content);
		}
		return true;
	}
	return false;
}

function pdfshift_parser_add_object(&$xref_infos, &$pdf_content, $raw_content) {
	$posinsert = $xref_infos['startxref'];
	$object_number = $xref_infos['xref']['nb_obj'];
	$pos = $xref_infos['startxref'];

	$raw_content = "{$object_number} 0 obj\n" . rtrim($raw_content) . "\nendobj\n";
	$raw_length = strlen($raw_content);

	// on ajoute au contenu du PDF
	$pdf_content = substr_replace($pdf_content, $raw_content, $posinsert, 0);

	// on met a jour les infos xref
	$xref_infos['xref']['nb_obj']++;
	$xref_infos['xref']['lines'][$object_number + 1] = str_pad($posinsert, 10, 0, STR_PAD_LEFT) .  " 00000 n";
	$xref_infos['startxref'] += $raw_length;
	pdfshift_parser_rewrite_xref_trailer_end($xref_infos, $pdf_content);
	return $object_number;
}

function pdfshift_parser_rewrite_xref_trailer_end(&$xref_infos, &$pdf_content) {
	$raw_trailer = $xref_infos['raw']['trailer'];
	$nb_obj = $xref_infos['xref']['nb_obj'];

	// mettre a jour le size dans le trailer
	if (preg_match(",/Size\b.*\n,", $raw_trailer, $m)) {
		$raw_trailer = str_replace($m[0], "/Size $nb_obj\n", $raw_trailer);
	}

	$details['raw']['xref'] = "xref\n"
		. $xref_infos['xref']['offset'] . ' ' . $nb_obj . "\n"
		. implode("\n", $xref_infos['xref']['lines']) . "\n";
	$details['raw']['trailer'] = rtrim($raw_trailer) . "\n";
	$details['raw']['tail'] = $details['raw']['xref']
		. "trailer\n"
		. trim($raw_trailer) . "\n"
		. "startxref\n"
		. $xref_infos['startxref'] . "\n"
		. "%%EOF";

	$pdf_content = substr_replace(
		$pdf_content,
		$details['raw']['tail'],
		$xref_infos['startxref'],
		strlen($pdf_content) - $xref_infos['startxref']
	);
}
