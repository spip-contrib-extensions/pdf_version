<?php

/**
 * Plugin PDF_VERSION v3
 * Licence GPL (c) 2016-2024 Cedric
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Appel de wkhtmltopdf par ligne de commande
 * @param string $html_file
 * @param string $pdf_file
 * @param array $args
 */
function pdf_version_services_wkhtmltopdf_exec_dist($html_file, $pdf_file, $args = []) {

	include_spip('inc/config');
	$logfile = 'pdfv_wkhtmltopdf_exec';

	if (defined('_PDF_VERSION_WKHTMLTOPDF_PATH')) {
		$wkhtmltopdf_path = _PDF_VERSION_WKHTMLTOPDF_PATH;
	} else {
		$wkhtmltopdf_path = lire_config('pdf_version/wkhtmltopdf_path', '/usr/local/bin/wkhtmltopdf');
	}

	if (!empty($wkhtmltopdf_path)) {

		// Appel de wkhtmltopdf en ligne de commande
		// on peut avoir plusieurs exec possibles

		$wkhtmltopdf_path = trim($wkhtmltopdf_path);
		$wkhtmltopdf_path_multiples = explode("\n", $wkhtmltopdf_path);
		$wkhtmltopdf_path_multiples = array_map('trim', $wkhtmltopdf_path_multiples);
		$wkhtmltopdf_path_multiples = array_filter($wkhtmltopdf_path_multiples);

		// todo : substitution des variables dans le header et footer
		/*
		Footers And Headers:
		  Headers and footers can be added to the document by the --header-* and
		  --footer* arguments respectfully.  In header and footer text string supplied
		  to e.g. --header-left, the following variables will be substituted.

		   * [page]       Replaced by the number of the pages currently being printed
		   * [frompage]   Replaced by the number of the first page to be printed
		   * [topage]     Replaced by the number of the last page to be printed
		   * [webpage]    Replaced by the URL of the page being printed
		   * [section]    Replaced by the name of the current section
		   * [subsection] Replaced by the name of the current subsection
		   * [date]       Replaced by the current date in system local format
		   * [isodate]    Replaced by the current date in ISO 8601 extended format
		   * [time]       Replaced by the current time in system local format
		   * [title]      Replaced by the title of the of the current page object
		   * [doctitle]   Replaced by the title of the output document
		   * [sitepage]   Replaced by the number of the page in the current site being converted
		   * [sitepages]  Replaced by the number of pages in the current site being converted
		 */
		$vars = [
			'@page@' => '[page]',
			'@total_page@' => '[topage]',
		];

		if (isset($args['--header-html'])) {
			lire_fichier($args['--header-html'], $html);
			$html = pdf_version_renomme_variables($vars, $html);
			ecrire_fichier($args['--header-html'], $html);
		}
		if (isset($args['--footer-html'])) {
			lire_fichier($args['--footer-html'], $html);
			$html = pdf_version_renomme_variables($vars, $html);
			ecrire_fichier($args['--footer-html'], $html);
		}

		$args[] = $html_file;
		$args[] = $pdf_file;

		$cmd = '';
		foreach ($args as $k => $v) {
			if (!is_numeric($k)) {
				$cmd .= " $k";
			}
			$cmd .= ' ' . escapeshellarg($v);
		}

		$cmd .= '  2>&1';

		foreach ($wkhtmltopdf_path_multiples as $count => $wkhtmltopdf_path) {
			$cmd_exec = $wkhtmltopdf_path . $cmd;
			$prologue = ($count == 0 ? '' : 'Essai ' . ($count + 1) . ': ');
			spip_log($prologue . $cmd_exec, $logfile);

			$output = [];
			$return_var = 0;
			exec($cmd_exec, $output, $return_var);
			spip_log(implode("\n", $output), $logfile);

			if (!$return_var and file_exists($pdf_file) and filesize($pdf_file)) {
				return true;
			}

			spip_log("Erreur $return_var", $logfile . _LOG_ERREUR);
		}

		// si on arrive la c'est un echec apres avoir essaye toutes les variantes
		return false;
	}

	spip_log('service wkhtmltopdf_exec mal configure', $logfile . _LOG_ERREUR);
	return false;
}
