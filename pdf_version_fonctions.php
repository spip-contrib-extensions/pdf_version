<?php

/**
 * Plugin PDF_VERSION v3
 * Licence GPL (c) 2016-2024 Cedric
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

function pdf_version_autoriser($flux) {

	return $flux;
}

/**
 * Fonction autoriser voirpdfversion generique
 * qui bloque les bot (franchement, ils n'ont rien a faire la)
 * et qui delegue a autoriser (voir) pour les humains
 *
 * Permet d'inserer des autorisations specifiques ou objet par objet
 *
 * @param $faire
 * @param $type
 * @param $id
 * @param $qui
 * @param $opt
 * @return bool
 */
function autoriser_voirpdfversion_dist($faire, $type, $id, $qui, $opt) {

	if (_IS_BOT) {
		return false;
	}
	return autoriser('voir', $type, $id, $qui, $opt);
}

/**
 * Lister les services dispnonibles
 * @return array
 */
function pdf_version_lister_services() {
	static $services;
	if (is_null($services)) {
		$services = [];

		// detection ou declaration pour permettre l'extension
		$services_php = find_all_in_path("pdf_version/services/", "/\w+.php$");
		foreach ($services_php as $php_basename) {
			$php_basename = basename($php_basename, '.php');
			if (strpos($php_basename, 'wkhtmltopdf_') === 0) {
				// legacy : les 2 services wkhtmltopdf ne reférencaient que leur mode d'utilisation exec/http
				// car il n'y avait pas d'autre service
				array_unshift($services, substr($php_basename, 12));
			} else {
				$services[] = $php_basename;
			}
		}
	}

	return $services;
}

/**
 * URL du PDF d'un objet
 * @param int $id_objet
 * @param string $objet
 * @return string
 */
function generer_url_pdf_version($id_objet, $objet) {

	$objet_composition = explode('-', $objet, 2);
	$objet = objet_type(array_shift($objet_composition));
	$composition = '';
	if (count($objet_composition)) {
		$composition = end($objet_composition);
	}

	if (strpos($id_objet, '=') !== false) {
		list($cle, $id_objet) = explode('=', $id_objet, 2);
		$cle = preg_replace(',\W,', '_', $cle);
		$id_objet = '-' . $cle . '-' . trim(strval($id_objet));
	} else {
		$id_objet = intval($id_objet);
		if ($id_objet < 0) {
			// double -- pour ne pas perdre le signe moins
			$id_objet = "-{$id_objet}";
		}
	}

	return _DIR_RACINE
		. 'pdf_version.api/objet/'
		. objet_type($objet)
		. ($composition ? "-{$composition}" : '')
		. '-' . $id_objet . '.pdf';
}

/**
 * Compile la balise `#URL_PDF_VERSION` qui génère l'URL de la version PDF d'un objet
 *
 * @balise
 * @example
 *     ```
 *     #URL_PDF_VERSION : renvoie l’url pour l’objet de la boucle en cours
 *     #URL_PDF_VERSION{article,123}
 *     ```
 * @param Champ $p
 *     Pile au niveau de la balise
 * @return Champ
 *     Pile complétée par le code à générer
 */
function balise_URL_PDF_VERSION_dist($p) {

	if ($_objet = interprete_argument_balise(1, $p)) {
		$_id_objet = interprete_argument_balise(2, $p);
	} elseif ($id_boucle = index_boucle($p)) {
		$_id_objet = champ_sql($p->boucles[$id_boucle]->primary, $p);
		$_objet = "objet_type('" . $p->boucles[$id_boucle]->id_table . "')";
	} else {
		$msg = ['zbug_champ_hors_boucle', ['champ' => ' URL_PDF_VERSION']];
		erreur_squelette($msg, $p);
		$p->code = "''";
		return $p;
	}

	$p->code = "generer_url_pdf_version($_id_objet, $_objet)";
	$p->interdire_scripts = false;
	return $p;
}


/**
 * URL du PDF d'une page
 * @param string $page
 * @param string $query_string
 * @return string
 */
function generer_url_pdf_page($page, $query_string) {

	return generer_url_pdf_version_api('page', $page, $query_string);
}


/**
 * URL du PDF d'une page
 * @param string $methode
 * @param string $quoi
 * @param string $args
 * @return string
 */
function generer_url_pdf_version_api($methode, $quoi, $args) {

	if ($methode === 'objet') {
		return generer_url_pdf_version($args, $quoi);
	}

	$methode = preg_replace(',\W,', '', $methode);
	$quoi = preg_replace(',[^\w-],', '_', $quoi);
	if (is_array($args)) {
		$args = http_build_query($args);
	} else {
		$args = ltrim(trim($args), '?');
	}

	return _DIR_RACINE
		. 'pdf_version.api/'
		. $methode . '/'
		. $quoi
		. '.pdf'
		. ($args ? "?$args" : '');
}


/**
 * Compile la balise `#URL_PDF_VERSION_API` qui génère l'URL de la version PDF d'une page
 *
 * @balise
 * @example
 *     ```
 *     #URL_PDF_VERSION_API{articles,statut=publie}
 *     ```
 * @param Champ $p
 *     Pile au niveau de la balise
 * @return Champ
 *     Pile complétée par le code à générer
 */
function balise_URL_PDF_VERSION_API_dist($p) {

	$_methode = interprete_argument_balise(1, $p);
	$_quoi = interprete_argument_balise(2, $p);
	$_arg = interprete_argument_balise(3, $p);
	$p->code = "generer_url_pdf_version_api($_methode, $_quoi, $_arg)";
	$p->interdire_scripts = false;
	return $p;
}

/**
 * Compile la balise `#URL_PDF_PAGE` qui génère l'URL de la version PDF d'une page
 *
 * @balise
 * @example
 *     ```
 *     #URL_PDF_PAGE{articles,statut=publie}
 *     ```
 * @param Champ $p
 *     Pile au niveau de la balise
 * @return Champ
 *     Pile complétée par le code à générer
 */
function balise_URL_PDF_PAGE_dist($p) {

	$_page = interprete_argument_balise(1, $p);
	$_query_string = interprete_argument_balise(2, $p);
	$p->code = "generer_url_pdf_page($_page, $_query_string)";
	$p->interdire_scripts = false;
	return $p;
}


/**
 * Effacer les pdf generes d'un objet, y compris les variantes
 * @param $objet
 * @param $id_objet
 */
function pdf_version_effacer_pdf($objet, $id_objet) {

	// supprimer la version PDF de l'objet si elle existe
	if (!function_exists('pdf_version_dir')){
		include_spip('inc/pdf_version');
	}
	if (file_exists($f = pdf_version_dir() . $objet . '-' . $id_objet . '.pdf')) {
		@unlink($f);
	}
	$variantes = glob(pdf_version_dir() . $objet . '-*-' . $id_objet . '.pdf');
	foreach ($variantes as $variante) {
		@unlink($variante);
	}
}


/**
 * Pipeline post_edition : supprimer la version PDF existante d'un objet modifie
 * @param $flux
 * @return mixed
 */
function pdf_version_post_edition($flux) {

	if (
		isset($flux['args']['table'])
		and $table = $flux['args']['table']
		and $id_objet = $flux['args']['id_objet']
	) {
		$objet = objet_type($table);
		pdf_version_effacer_pdf($objet, $id_objet);
	}

	return $flux;
}

/**
 * Ajouter un lien 'Voir la version PDF' sur les objets pour lesquels c'est possible
 *
 * @param array $flux
 * @return array
 */
function pdf_version_boite_infos($flux) {

	if (
		$objet = $flux['args']['type']
		and $id_objet = $flux['args']['id']
	) {
		if (
			trouver_fond('pdf_version/' . $objet)
			and objet_test_si_publie($objet, $id_objet)
		) {
			if (autoriser('voirpdfversion', $objet, $id_objet)) {
				include_spip('inc/presentation');
				$url = generer_url_pdf_version($id_objet, $objet);
				// forcer la mise a jour
				$url = parametre_url($url, 'var_mode', 'recalcul');
				$flux['data'] .= icone_horizontale(_T('pdf_version:icone_voir_pdf_version'), $url, 'pdf_version');
			}
		}
	}

	return $flux;
}
