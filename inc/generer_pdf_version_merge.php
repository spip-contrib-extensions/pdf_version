<?php

/**
 * Plugin PDF_VERSION v3
 * Licence GPL (c) 2016-2024 Cedric
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * @param string $fond_liste
 * @param array $contexte
 * @param string $pdf_file
 * @param bool $force_recalcul
 * @return bool
 */
function inc_generer_pdf_version_merge_dist($fond_liste, $contexte, $pdf_file, $force_recalcul = false) {

	$fond = 'pdf_version/' . $fond_liste . '.txt';
	// si on ne sait pas generer le PDF pour cet objet, on ne fait rien
	if (!trouver_fond($fond)) {
		return false;
	}

	$liste = recuperer_fond($fond, $contexte);
	$liste = explode("\n", $liste);
	$liste = array_map('trim', $liste);
	$liste = array_filter($liste);

	if (!$liste) {
		spip_log("generer_pdf_version_merge: Rien a generer pour $fond_liste " . json_encode($contexte), 'pdf_version' . _LOG_ERREUR);
		return false;
	}

	include_spip('inc/pdf_version');

	$hash = [];
	foreach ($liste as $fileName) {
		$hash[] = [$fileName, filemtime($fileName)];
	}
	$hash = json_encode($hash);
	#spip_log("generer_pdf_version_merge: hash $hash", 'pdf_version' . _LOG_DEBUG);
	$hash = md5($hash);

	if (!file_exists($pdf_file)) {
		$force_recalcul = true;
	}
	if (!$force_recalcul) {
		$hash_existant = pdf_version_read_pdf_hash($pdf_file);
		if ($hash === $hash_existant) {
			spip_log("generer_pdf_version_merge: hash $hash inchange pour fichier $pdf_file / PAS de regeneration du PDF", 'pdf_version' . _LOG_DEBUG);
			return true;
		}
		else {
			spip_log("generer_pdf_version_merge: hash $hash != $hash_existant pour fichier $pdf_file => REGENERATION du PDF", 'pdf_version' . _LOG_DEBUG);
		}
	}

	include_spip('lib/php-pdf-merge/vendor/autoload');

	// create merger instance
	$merger = new \Jurosh\PDFMerge\PDFMerger();

	// do work
	$tmp_file = $pdf_file . '.tmp';
	$handle = fopen($tmp_file, 'w');
	fclose($handle);
	foreach ($liste as $fileName) {
		if (file_exists($fileName)) {
			$merger->addPDF($fileName, 'all', 'auto');
		}
	}
	$merger->merge('file', $tmp_file);

	if (file_exists($tmp_file) && filesize($tmp_file)) {
		@rename($tmp_file, $pdf_file);
		pdf_version_write_pdf_hash($pdf_file, $hash);
		return true;
	}

	return false;
}
