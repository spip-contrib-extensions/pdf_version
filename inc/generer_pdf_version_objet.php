<?php

/**
 * Plugin PDF_VERSION v3
 * Licence GPL (c) 2016-2024 Cedric
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * @param string $objet
 *   objet ou objet-composition
 * @param int $id_objet
 * @param string $pdf_file
 * @param bool $force_recalcul
 * @return bool
 */
function inc_generer_pdf_version_objet_dist($objet, $id_objet, $pdf_file, $force_recalcul = false) {

	$composition = '';
	if (strpos($objet, '-')) {
		$objet_composition = explode('-', $objet, 2);
		list($objet, $composition) = $objet_composition;
	}

	// preambule : si un pdf joint avec le credit='pdf_version' est associe a l'objet
	// (ou credit='pdf_version-xxx' avec xxx la composition si on a une composition)
	// on l'utilise comme version PDF manuelle
	// (si ce n'est pas un document distant)
	$credits = 'pdf_version' . ($composition ? "-{$composition}" : '');
	$pdf_join = sql_fetsel('*', 'spip_documents as D JOIN spip_documents_liens as L ON D.id_document=L.id_document', 'D.extension=' . sql_quote('pdf') . ' AND D.credits=' . sql_quote($credits) . ' AND L.id_objet=' . intval($id_objet) . ' AND L.objet=' . sql_quote($objet), '', 'D.id_document DESC', '0,1');
	if ($pdf_join) {
		include_spip('inc/documents');
		if (
			$f = get_spip_doc($pdf_join['fichier'])
			and file_exists($f)
		) {
			if (
				!file_exists($pdf_file)
				or filesize($f) !== filesize($pdf_file)
				or md5_file($f) !== md5_file($pdf_file)
			) {
				@copy($f, $pdf_file);
			}
			return true;
		}
	}

	$primary = id_table_objet($objet);

	$fond = 'pdf_version/' . $objet;
	if ($composition and trouver_fond($f = "{$fond}-{$composition}")) {
		$fond = $f;
	}
	// si on ne sait pas generer le PDF pour cet objet, on ne fait rien
	elseif (!trouver_fond($fond)) {
		spip_log("generer_pdf_version_fond: $fond introuvable pour ($objet, $id_objet, $pdf_file)", 'pdf_version' . _LOG_DEBUG);
		return false;
	}

	$variantes = [$objet, 'objet'];
	if ($composition) {
		array_unshift($variantes, "{$objet}-{$composition}");
	}

	$fond_header = '';
	foreach ($variantes as $variante) {
		if (trouver_fond($f = "pdf_version/{$variante}-header")) {
			$fond_header = $f;
			break;
		}
	}
	$fond_footer = '';
	foreach ($variantes as $variante) {
		if (trouver_fond($f = "pdf_version/{$variante}-footer")) {
			$fond_footer = $f;
			break;
		}
	}

	$contexte = [$primary => $id_objet];
	if ($composition) {
		$contexte['composition'] = $composition;
	}

	$generer_pdf_version_fond = charger_fonction('generer_pdf_version_fond', 'inc');
	return $generer_pdf_version_fond($pdf_file, $fond, $contexte, $fond_header, $fond_footer, "$objet-$id_objet", $force_recalcul);
}
