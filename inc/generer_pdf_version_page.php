<?php

/**
 * Plugin PDF_VERSION v3
 * Licence GPL (c) 2016-2024 Cedric
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * @param string $page
 *   objet ou objet-composition
 * @param array $contexte
 * @param string $pdf_file
 * @param bool $force_recalcul
 * @return bool
 */
function inc_generer_pdf_version_page_dist($page, $contexte, $pdf_file, $force_recalcul = false) {

	$composition = '';

	$fond = 'pdf_version/' . $page;
	// si on ne sait pas generer le PDF pour cet objet, on ne fait rien
	if (!trouver_fond($fond)) {
		return false;
	}

	$variantes = [$page, 'page'];
	$fond_header = '';
	foreach ($variantes as $variante) {
		if (trouver_fond($f = "pdf_version/{$variante}-header")) {
			$fond_header = $f;
			break;
		}
	}
	$fond_footer = '';
	foreach ($variantes as $variante) {
		if (trouver_fond($f = "pdf_version/{$variante}-footer")) {
			$fond_footer = $f;
			break;
		}
	}

	$generer_pdf_version_fond = charger_fonction('generer_pdf_version_fond', 'inc');
	return $generer_pdf_version_fond($pdf_file, $fond, $contexte, $fond_header, $fond_footer, "page-$page", $force_recalcul);
}
