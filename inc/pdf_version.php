<?php

/**
 * Plugin PDF_VERSION v3
 * Licence GPL (c) 2016-2024 Cedric
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

define('_DIR_PDF_VERSION', _DIR_IMG . 'pdf_version/');

function pdf_version_dir() {
	if (!is_dir(_DIR_PDF_VERSION)) {
		sous_repertoire(_DIR_PDF_VERSION);
		pdf_version_gerer_htaccess(true);
	}
	return _DIR_PDF_VERSION;
}

/**
 * Fonction facilitatrice pour generer le PDF d'un objet
 *
 * @param string $objet
 *   objet ou objet-composition
 * @param int $id_objet
 * @param string $pdf_file
 * @param bool $force_recalcul
 * @return mixed
 */
function generer_pdf_version_objet($objet, $id_objet, $pdf_file, $force_recalcul = false) {

	$composition = '';
	if (strpos($objet, '-')) {
		$objet_composition = explode('-', $objet, 2);
		list($objet, $composition) = $objet_composition;
	}

	if (
		(!$composition or !$generer_pdf_version_objet = charger_fonction('generer_pdf_version_' . $objet . '_' . $composition, 'inc', true))
		and !$generer_pdf_version_objet = charger_fonction('generer_pdf_version_' . $objet, 'inc', true)
	) {
		$generer_pdf_version_objet = charger_fonction('generer_pdf_version_objet', 'inc');
	}

	return $generer_pdf_version_objet($objet . ($composition ? '-' . $composition : ''), $id_objet, $pdf_file, $force_recalcul);
}


/**
 * Fonction facilitatrice pour generer le PDF d'une page
 *
 * @param string page
 *   objet ou objet-composition
 * @param array $contexte
 * @param string $pdf_file
 * @param bool $force_recalcul
 * @return mixed
 */
function generer_pdf_version_page($page, $contexte, $pdf_file, $force_recalcul = false) {
	if (!$generer_pdf_version_page = charger_fonction('generer_pdf_version_page_' . $page, 'inc', true)) {
		$generer_pdf_version_page = charger_fonction('generer_pdf_version_page', 'inc');
	}

	return $generer_pdf_version_page($page, $contexte, $pdf_file, $force_recalcul);
}


/**
 * Fonction facilitatrice pour generer le merge PDF d'une liste
 *
 * @param string $liste
 * @param array $contexte
 * @param string $pdf_file
 * @param bool $force_recalcul
 * @return mixed
 */
function generer_pdf_version_merge($liste, $contexte, $pdf_file, $force_recalcul = false) {
	if (!$generer_pdf_version_merge = charger_fonction('generer_pdf_version_merge_' . $liste, 'inc', true)) {
		$generer_pdf_version_merge = charger_fonction('generer_pdf_version_merge', 'inc');
	}

	return $generer_pdf_version_merge($liste, $contexte, $pdf_file, $force_recalcul);
}


/**
 * on essaye de poser un htaccess rewrite global sur IMG/
 * si fonctionne on gardera des ulrs de document permanente
 * si ne fonctionne pas on se rabat sur creer_htaccess du core
 * qui pose un deny sur chaque sous repertoire de IMG/
 *
 * https://code.spip.net/@gerer_htaccess
 *
 * @param bool $active
 * @return bool
 */
function pdf_version_gerer_htaccess($active = true) {

	if (!$active) {
		spip_unlink(_DIR_PDF_VERSION . _ACCESS_FILE_NAME);
		return false;
	}
	else {
		$rewrite = <<<rewrite
RewriteEngine On
RewriteRule ^(.*)\.pdf$     ../../spip.php?action=api_pdf_version&arg=$1 [QSA,L,skip=100]
rewrite;

		ecrire_fichier(_DIR_PDF_VERSION . _ACCESS_FILE_NAME, $rewrite);

		return true;
	}
}



/**
 * Affiche une page indiquant un document introuvable ou interdit
 *
 * @param string $status
 *     Numero d'erreur (403 ou 404)
 * @return void
**/
function pdf_version_afficher_erreur_document($status = 404) {

	switch ($status) {
		case 304:
			include_spip('inc/headers');
			// not modified : sortir de suite !
			http_response_code(304);
			exit;

		case 403:
			include_spip('inc/minipres');
			echo minipres('', '', '', true);
			break;

		case 404:
			http_response_code(404);
			include_spip('inc/minipres');
			echo minipres(_T('erreur') . ' 404', _T('medias:info_document_indisponible'), '', true);
			break;
	}
}


/**
 * Lire le hash du PDF, qui est ecrit sauvagement en fin de fichier
 * @param $pdf_file
 * @return false|string
 */
function pdf_version_read_pdf_hash($pdf_file) {
	$fp = fopen($pdf_file, 'r');
	fseek($fp, -100, SEEK_END);

	$file_end = fread($fp, 100);

	if (($p = strpos($file_end, 'SPIP hash:') !== false)) {
		$file_end = explode('SPIP hash:', $file_end, 2);
		$hash = end($file_end);
		$hash = substr($hash, 0, 32);
		if (preg_match(',^[0-9a-f]*$,', $hash)) {
			return $hash;
		}
	}

	return '';
}

/**
 * Ecrire le hash du PDF : en fin de fichier apres le %%EOF du coup c'est totalement ignore par les parseurs PDF
 * @param $pdf_file
 * @param $hash
 */
function pdf_version_write_pdf_hash($pdf_file, $hash) {

	file_put_contents($pdf_file, "\nSPIP hash:$hash\n", FILE_APPEND);
	return;
}

/**
 * Fonction iterative pour generer un merge de N PDF qui prend en charge le timeout et se relance toutes les 25s
 * Pour que ça marche il faut que les fonctions appelees pour le build aient un cache d'au moins quelques minutes
 * ce qui permet a chaque iteration de passer tres vites sur les X premiers PDF deja prepares et d'avancer dans la liste
 *
 * @param array $liste_to_build
 *   tableau de fonction, typiquement : [ ['action_api_pdf_version_objet', 'article-1', []], ['action_api_pdf_version_objet', 'article-2', []],...]
 *
 * @return array
 *   liste des PDFs generes
 */
function pdf_version_build_pdf_liste($liste_to_build) {
	$time_limit = $_SERVER['REQUEST_TIME'] + 25;

	$var_build_start = null;
	if (!empty($_REQUEST['var_build_step'])) {
		$var_build_start = $_REQUEST['var_build_step'];
	}

	$liste_pdf_files = [];
	foreach ($liste_to_build as $k => $build_args) {
		$function = array_shift($build_args);
		$liste_pdf_files[] = call_user_func_array($function, $build_args);
		if (time() > $time_limit) {
			$url_redirect = parametre_url(self(), 'var_build_step', $k, '&');
			$url_redirect = parametre_url($url_redirect, 'var_t', time(), '&');
			return sinon_interdire_acces(false, $url_redirect);
		}
	}
	return $liste_pdf_files;
}

/**
 * Charger l'implémentation d'un service ou la fonction service_inconnu sinon
 * @param string|null $methode
 * @return string
 */
function pdf_version_charger_service($methode = null) {
	include_spip('inc/config');
	if (empty($methode)) {
		$methode = lire_config('pdf_version/methode', 'exec');
	}

	switch ($methode) {
		case 'exec':
			$methode = 'wkhtmltopdf_exec';
			break;
		case 'http':
			$methode = 'wkhtmltopdf_http';
			break;
	}

	if ($pdf_version_services = charger_fonction($methode, 'pdf_version/services', true)) {
		return $pdf_version_services;
	}

	spip_log("pdf_version_charger_service : la methode $methode n'est pas implémentée", 'pdf_version' . _LOG_ERREUR);
	return charger_fonction('inconnu', 'pdf_version/services');
}

/**
 * Service inconnue
 * @param string $html_file
 * @param string $pdf_file
 * @param array $args
 */
function pdf_version_services_inconnu_dist($html_file, $pdf_file, $args = []) {
	return false;
}


/**
 * Renommer les variables @page@ du header/footer dans la syntaxe propre à chaque service
 * @param array $vars
 * @param string $html
 * @return string
 */
function pdf_version_renomme_variables($vars, $html) {
	foreach ($vars as $spip_name => $service_name) {
		if (strpos($html, $spip_name) !== false) {
			$html = str_replace($spip_name, $service_name, $html);
		}
	}

	return $html;
}

/**
 * rechercher les variables dans le <head> du html principal
 * elles peuvent etre injectees sous forme de commentaire
 * 	<!--
 *	 --margin-top=45mm
 *	-->
 *
 * @param array $vars
 * @param string &$html
 * @param bool $clean_html
 * @return array
 */
function pdf_version_collecter_variables_head($vars, &$html, $clean_html = false) {

	$args = [];
	$head = explode('</head>', $html);
	$head = reset($head);
	$head_clean = $head;
	foreach ($vars as $varname) {
		if (
			strpos($head, "{$varname}=") !== false
			and preg_match(",{$varname}=(.*)\n,Uims", $head, $m)
		) {
			$v = trim($m[1]);
			if (in_array(substr($v, 0, 1), ['"', "'"]) and substr($v, 0, 1) === substr($v, -1, 1)) {
				$v = substr($v, 1, -1);
			}
			$args[$varname] = $v;
			if ($clean_html) {
				$head_clean = str_replace($m[0], '', $head_clean);
			}
		}
	}
	if ($clean_html and $head_clean !== $head) {
		$html = str_replace($head, $head_clean, $html);
	}

	return $args;
}
