<?php

/**
 * Plugin PDF_VERSION v3
 * Licence GPL (c) 2016-2024 Cedric
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * @param string $pdf_file
 * @param string $fond
 * @param array $contexte
 * @param string $fond_header
 * @param string $fond_footer
 * @param string $racine_tmp_file
 * @param bool $force_recalcul
 * @return bool
 */
function inc_generer_pdf_version_fond_dist($pdf_file, $fond, $contexte, $fond_header = '', $fond_footer = '', $racine_tmp_file = '', $force_recalcul = false) {

	include_spip('inc/pdf_version');
	$html = recuperer_fond($fond, $contexte);

	$header = '';
	if ($fond_header) {
		$header = recuperer_fond($fond_header, $contexte);
	}
	if (stripos($header, '</body>') === false) {
		$header = "<!DOCTYPE html><html><head></head><body style='margin: 0;padding: 0;'>$header</body></html>";
	}

	$footer = '';
	if ($fond_footer) {
		$footer = recuperer_fond($fond_footer, $contexte);
	}
	if (stripos($footer, '</body>') === false) {
		$footer = "<!DOCTYPE html><html><head></head><body style='margin: 0;padding: 0;'>$footer</body></html>";
	}

	$tmp_dir = sous_repertoire(_DIR_TMP, 'pdf_version');
	include_spip('inc/acces');
	$tmp_token = substr(md5($fond . ':' . json_encode($contexte) . ':' . creer_uniqid()), 0, 8);

	if (!$racine_tmp_file) {
		$racine_tmp_file = basename($fond);
	}

	$tmp_file = $tmp_dir . $racine_tmp_file . "-$tmp_token.html";
	$tmp_file_footer = $tmp_dir . $racine_tmp_file . "-$tmp_token-footer.html";
	$tmp_file_header = $tmp_dir . $racine_tmp_file . "-$tmp_token-header.html";

	ecrire_fichier($tmp_file, $html);
	ecrire_fichier($tmp_file_footer, $footer);
	ecrire_fichier($tmp_file_header, $header);

	$hash = json_encode([
		'html' => md5(pdf_version_nettoyer_html_pour_md5($html)),
		'header' => md5(pdf_version_nettoyer_html_pour_md5($header)),
		'footer' => md5(pdf_version_nettoyer_html_pour_md5($footer))
	]);
	#spip_log("$pdf_file : $hash", 'pdf_version' . _LOG_DEBUG);
	$hash = md5($hash);

	if (!file_exists($pdf_file)) {
		$force_recalcul = true;
	}
	if (!$force_recalcul) {
		$hash_existant = pdf_version_read_pdf_hash($pdf_file);
		if ($hash === $hash_existant) {
			spip_log("generer_pdf_version_fond: hash $hash inchange pour fichier $pdf_file / PAS de regeneration du PDF", 'pdf_version' . _LOG_DEBUG);
			@unlink($tmp_file);
			@unlink($tmp_file_footer);
			@unlink($tmp_file_header);
			return true;
		}
		else {
			spip_log("generer_pdf_version_fond: hash $hash != $hash_existant pour fichier $pdf_file => REGENERATION du PDF", 'pdf_version' . _LOG_DEBUG);
		}
	}

	$pdf_version_service = pdf_version_charger_service();

	$args = [
		'--margin-left' => '15mm',
		'--margin-right' => '15mm',
		'--margin-top' => '15mm',
		'--margin-bottom' => '30mm',
		'--header-html' => $tmp_file_header,
		'--header-spacing' => '5',
		'--footer-html' => $tmp_file_footer,
		'--footer-spacing' => '5',
		'--page-size' => 'A4',
		'--orientation' => 'Portrait',
		'--dpi' => '72',
	];

	$perso = pdf_version_collecter_variables_head(array_keys($args), $html);
	if (!empty($perso)) {
		$args = array_merge($args, $perso);
	}

	$res = $pdf_version_service($tmp_file, $pdf_file, $args);

	if (!$res and defined('_PDF_VERSION_DEBUG')) {
		$tmp_dir_failed = sous_repertoire($tmp_dir, 'fail');
		$tmp_dir_failed = sous_repertoire($tmp_dir_failed, $tmp_token);
		@rename($tmp_file, $tmp_dir_failed . basename($tmp_file));
		@rename($tmp_file_footer, $tmp_dir_failed . basename($tmp_file_footer));
		@rename($tmp_file_header, $tmp_dir_failed . basename($tmp_file_header));
	}
	else {
		@unlink($tmp_file);
		@unlink($tmp_file_footer);
		@unlink($tmp_file_header);
	}

	if ($res) {
		pdf_version_write_pdf_hash($pdf_file, $hash);
		return true;
	}
	else {
		return false;
	}
}

/**
 * Nettoyer le html avant de calculer son hash
 * notamment les contextes ajax qui changent sans arret et sont totalement inutiles dans un PDF
 *
 *
 * @param string $html
 * @return string|string[]
 */
function pdf_version_nettoyer_html_pour_md5($html) {
	if (strpos($html, 'data-ajax-env=') !== false) {
		$html_clean = preg_replace(",(<div[^>]*)\sdata-ajax-env='[^'>]*',Uims", "\\1", $html);
		if ($html_clean) {
			return $html_clean;
		}
	}
	return $html;
}

/**
 * Source : Lib MPDF
 *
 * @param $s
 * @return string
 */
function pdf_version_UTF16BEtextstring($s) {
	$s = pdf_version_UTF8ToUTF16BE($s, true);
	/* -- ENCRYPTION -- */
	/*
	if ($this->encrypted) {
		$s = $this->_RC4($this->_objectkey($this->_current_obj_id), $s);
	}
	*/
	/* -- END ENCRYPTION -- */
	return '(' . pdf_version_escape($s) . ')';
}

/**
 * Converts UTF-8 strings to UTF16-BE.
 * Source : Lib MPDF
 * @param $str
 * @param bool $setbom
 * @return string
 */
function pdf_version_UTF8ToUTF16BE($str, $setbom = true) {
	/*
	if ($this->checkSIP && preg_match("/([\x{20000}-\x{2FFFF}])/u", $str)) {
		if (!in_array($this->currentfontfamily, array('gb', 'big5', 'sjis', 'uhc', 'gbB', 'big5B', 'sjisB', 'uhcB', 'gbI', 'big5I', 'sjisI', 'uhcI',
				'gbBI', 'big5BI', 'sjisBI', 'uhcBI'))) {
			$str = preg_replace("/[\x{20000}-\x{2FFFF}]/u", chr(0), $str);
		}
	}
	if ($this->checkSMP && preg_match("/([\x{10000}-\x{1FFFF}])/u", $str)) {
		$str = preg_replace("/[\x{10000}-\x{1FFFF}]/u", chr(0), $str);
	}
	*/
	$outstr = ''; // string to be returned
	if ($setbom) {
		$outstr .= "\xFE\xFF"; // Byte Order Mark (BOM)
	}
	$outstr .= mb_convert_encoding($str, 'UTF-16BE', 'UTF-8');
	return $outstr;
}

/**
 * Source : Lib MPDF
 * @param $s
 * @return string
 */
function pdf_version_escape($s) {
	// the chr(13) substitution fixes the Bugs item #1421290.
	return strtr($s, [')' => '\\)', '(' => '\\(', '\\' => '\\\\', chr(13) => '\r']);
}
